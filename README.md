# Custom Framework Base #

Basis for building php framework.
Author: Eduard Samoilenko 

## What you need to know? ##

This repository is a practical lesson for building and modifying the author’s framework.

The view of the framework is presented in an introductory sample and is not recommended for practical use without knowledge of the object-oriented approach in programming.

The code is not final. Any part of the framework can be modified, improved or rewritten, supplemented with new libraries and approaches to construction.

## Functional parts ##

### Done ###

* Basic design uses Composer. Classes are built on Autoloader by the psr-0 standard. It is possible to connect packages from outside to the vendor folder.
* To output data, a Blade template engine is built in with access to all standard Laravel >=5.4 functions, caching works. Based on https://github.com/XiaoLer/blade .
* Added service method go redirect with configuration files.
* Built the basis for multilingualism using a config file.
* Other minor works.

### TODO ###

* Use a convenient builder for the database model.
* A new approach to building a permission system.
* Optimal route detection.
* Add logging methods.
* Embedding an environment configuration file.
* Implement caching using memcached or redis. Better optional.
* Update error handler with more information.
* Make an installer to deploy in a few clicks.
* Permanent code review
* Adding new bugs to fix later

## What i say? ##

For me, this project is very important, since it is an excellent practice and method for learning new knowledge, self-development. 

The development of the project takes place in his spare time. 

As a developer, I don’t think development is ideal and I recommend using this material for educational purposes, as a framework I recommend using Laravel Framework, Lumen Micro-Framework or other most common products.

## Сontact with me ##

I will be very happy for the feedback. Also new ideas and constructive criticism.

* LinkedIn profile: https://www.linkedin.com/in/eduard9969/
* Telegram profile: https://t.me/makkintosh
* Email inbox: Eduard9969@gmail.com


### Thank you for your time :) ###
