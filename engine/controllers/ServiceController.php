<?php

namespace engine\controllers;

use engine\core\Controller;
use engine\core\View;

/**
 * Class ServiceController
 * @package engine\controllers
 */
class ServiceController extends Controller
{

    /**
     * Go Action
     */
    public function goAction()
    {
        $alias      = $this->route['alias'] ?? null;    // get link alias
        $url_assoc  = $this->config('go_list');  // get allow links

        /*
         * Redirect to homepage, if link is not specified
         */
        if(empty($alias) || !key_exists($alias, $url_assoc))
            $this->view->redirect('/');

        $this->view->redirect_external($url_assoc[$alias]);
    }

    /**
     * Error Action
     * @throws \Throwable
     */
    public function errorAction()
    {
        $code = $this->route['code'] ?? null;
        if(empty($code)) exit();

        View::errorCode($code);
    }

}