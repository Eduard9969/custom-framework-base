<?php

  namespace engine\controllers;

  use engine\core\Controller;
  use engine\lib\assets\ExtractJsonContent;

  /**
 * Class MainController
 * @package engine\controllers
 */
class MainController extends Controller
{

    /**
    * Index Method
    */
    public function indexAction()
    {
        $this->view->assign('hello', 'Hello World!');

        $this->view->render('Main Page');
    }

}