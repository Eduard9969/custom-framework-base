<?php

namespace engine\core;

use engine\lib\Db;

/**
 * Class Model
 * @package engine\core
 */
abstract class Model
{

    public $db;

    /**
     * Model constructor.
     */
    public function __construct()
    {
        $this->db = new Db();
    }
}
