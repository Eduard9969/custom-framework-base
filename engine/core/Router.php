<?php

namespace engine\core;

use engine\core\View;

/**
 * Class Router
 * @package engine\core
 */
class Router extends Facade
{

    /**
     * @var array
     */
    protected $routes = [];

    /**
     * @var array
     */
    protected $params = [];

    /**
     * Router constructor.
     */
    function __construct()
    {
        /*
         * Get an array of available routes
         */
        $arr = $this->config('routes');

        foreach ($arr as $key => $val)
            $this->add($key, $val);
    }

    /**
     * Filling an routes array with a key
     * The key may be a regular expression
     *
     * @param $route
     * @param $params
     */
    public function add($route, $params)
    {
        $route = preg_replace('/{([a-z]+):([^\}]+)}/', '(?P<\1>\2)', $route);
        $route = '#^' . $route . "$#";

        $this->routes[$route] = $params;
    }

    /**
     * Checking the correctness of the request based on the routes config
     *
     * @return bool
     */
    public function match()
    {
        $url = trim(strtok($_SERVER['REQUEST_URI'], '?'), '/');

        foreach ($this->routes as $route => $params)
        {
            if(preg_match($route, $url, $matches))
            {
                foreach ($matches as $key => $match)
                {
                    if(is_string($key))
                    {
                        if(is_numeric($match))
                        {
                            $match = (int)$match;
                        }

                        $params[$key] = $match;
                    }
                }

                $this->params = $params;

                return true;
            }
        }

        return false;
    }

    /**
     * Starting request processing
     * @throws \Throwable
     */
    public function run()
    {
        if($this->match())
        {

            /*
             * If a stub is used, we interrupt further execution
             */
            if(defined('PLUG') && PLUG)
                View::enablePlug($this->params['controller']);

            /*
             * Connect the controller class and execute the called method
             *
             * The controller class has the form ClassName . Controller
             * The action method has the form ActionName . Action
             */
            $path = 'engine\controllers\\' . ucfirst($this->params['controller']) . 'Controller';

            if(class_exists($path))
            {
                $action = $this->params['action'] . 'Action';

                if(method_exists($path, $action))
                {
                    $controller = new $path($this->params);
                    $controller->$action();
                }
                else View::errorCode(404);

            }
            else View::errorCode(404);

        }
        else View::errorCode(404);

    }
}
