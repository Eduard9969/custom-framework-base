<?php

namespace engine\core;

use engine\core\View;
use engine\lib\Lang;

/**
 * Class Controller
 * @package engine\core
 */
abstract class Controller extends Facade
{

    /**
     * @var
     */
    public $route;

    /**
     * @var \engine\core\View
     */
    public $view;

    /**
     * @var
     */
    public $acl;

    /**
     * @var array
     */
    private $langs = [];

    /**
     * @var mixed
     */
    protected $model;

    /**
     * @var null
     */
    protected $lang;

    /**
     * Controller constructor.
     *
     * @param $route
     * @throws \Throwable
     */
    public function __construct($route)
    {
        $this->route = $route;

        /*
         * Permission check
         */
        if(!$this->checkAcl())
            View::errorCode(403);

        /*
         * Creating a view object
         */
        $this->view  = new View($route);

        /*
         * Setting the current language
         */
        $this->lang  = $this->setLang();

        /*
         * Creating a controller model object
         */
        $this->model = $this->loadModel($route['controller']);
    }

    /**
     * Download Controller Model
     *
     * @param $name
     * @return mixed
     */
    public function loadModel($name)
    {
        $class_name = 'engine\models\\' . ucfirst($name);

        /*
         * If the model is disabled or not found, then returns an empty object
         */
        if(!MODEL_ON || !class_exists($class_name))
            return new \stdClass();

        /*
         * Model object declarations if the model is present
         */
        return new $class_name;
    }

    /**
     * Setting the interface language
     *
     * @return null
     */
    public function setLang()
    {
        $mLang = new Lang();

        $check = $this->checkLangRequest();
        if(!empty($check))
            $this->view->redirect_previous();

        $langs = $this->config('langs');
        $mLang->setLangShortsTitle($langs);

        $request_array = $_SESSION ?? [];
        $mLang->setLangFromRequest($request_array);

        $lang = $mLang::getLang();

        /*
         * Set Language view
         */
        $this->view->assign('lang',  $lang);
        $this->view->assign('langs', $this->langs = $langs);

        return $lang;
    }

    /**
     * Get an array of languages
     *
     * @return mixed
     */
    protected function getLangs()
    {
        return $this->langs;
    }

    /**
     * Check for language changes
     *
     * @return mixed|null
     */
    private function checkLangRequest()
    {
        return isset($_REQUEST['lng']) ? $_SESSION['lang'] = $_REQUEST['lng'] : null;
    }

    /**
     * User Role Distribution
     *
     * @return bool
     */
    public function checkAcl()
    {
        /*
         * Controller Access Identifier File
         */
        $this->acl = $this->acl_config($this->route['controller']);
        if(empty($this->acl)) return false;

        /*
         * Access Groups
         */
        if($this->isAcl('all')) return true;

        return false;
    }

    /**
     * Verify user role
     *
     * @param $key
     * @return bool
     */
    public function isAcl($key)
    {
        return isset($this->acl[$key]) && in_array($this->route['action'], $this->acl[$key]);
    }

    /**
     * Check string for JSON type
     *
     * @param $string
     * @return bool
     */
    public function isJSON($string)
    {
        return ((is_string($string) && (is_object(json_decode($string)) || is_array(json_decode($string)))));
    }

    /**
     * Getting client IP address
     *
     * @return mixed
     */
    public function getIP()
    {
        return isset($_SERVER['HTTP_X_REAL_IP']) ? $_SERVER['HTTP_X_REAL_IP'] : $_SERVER['REMOTE_ADDR'];
    }

}