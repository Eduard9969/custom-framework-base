<?php


namespace engine\core;

/**
 * Class Facade
 * @package engine\core
 */
abstract class Facade
{

    /**
     * Include Config Data
     *
     * @param $name
     * @return array|mixed
     */
    protected function config($name)
    {
        return $this->include_file(DIR . '/engine/config/' . $name . '.php');
    }

    /**
     * Include Access Control List
     *
     * @param $name
     * @return array|mixed
     */
    protected function acl_config($name)
    {
        return $this->include_file(DIR . '/engine/acl/' . $name . '.php');
    }

    /**
     * Include File
     *
     * @param $file
     * @return array|mixed
     */
    private function include_file($file)
    {
        return file_exists($file) ? require_once $file : [];
    }

}