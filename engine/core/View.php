<?php

namespace engine\core;

use engine\lib\TimeOut;

/**
 * Class View
 * @package engine\core
 */
class View
{

    /**
     * @var string
     */
    public $path;

    /**
     * @var
     */
    public $route;

    /**
     * @var string
     */
    public $layout = 'default';

    /**
     * @var array
     */
    public $vars = [];

    /**
     * @var null
     */
    public $prev_url = null;

    /**
     * View constructor.
     * @param $route
     */
    public function __construct($route)
    {
        $this->route = $route;
        $this->path = $route['controller'] . '/' . $route['action'];

        /*
         * Previous page setup
         */
        if(isset($_SERVER['HTTP_REFERER']))
            $this->prev_url = $_SERVER['HTTP_REFERER'];
    }

    /**
     * Variable assignment for Blade engine
     * Сan be used in the controller for view template
     *
     * @param $var
     * @param $content
     *
     * @return mixed
     */
    public function assign($var, $content)
    {
        return $this->vars[$var] = $content;
    }

    /**
     * Page rendering
     *
     * @param string $title
     * @throws \Throwable
     */
    public function render($title = '')
    {
        /*
         * Get Global Factory object
         */
        global $factory;

        /*
         * Default variable assignment
         */
        $this->assign('title', $title);
        $this->assign('time_out', TimeOut::getTimeOut());

        $this->assign('controller', $this->route['controller']);
        $this->assign('action',     $this->route['action']);

        /*
         * Display template
         */
        echo $factory->make($this->path, $this->vars)->render();
    }

    /**
     * Error Handler Call
     *
     * @param $code
     * @throws \Throwable
     */
    public static function errorCode($code)
    {
        $view = new View(['controller' => 'errors', 'action' => 'index']);

        http_response_code($code);

        $view->assign('code', $code);
        $view->render('An error has occurred ' . $code . '.');

        exit();
    }

    /**
     * Call stub
     *
     * @param $controller
     * @throws \Throwable
     */
    public static function enablePlug($controller)
    {
        $view = new View(['controller' => 'plug', 'action' => 'index']);

        if($controller != 'main')
            $view->redirect('/', true);

        $view->render('A little patience');

        exit();
    }

    /**
     * Call redirection
     *
     * @param $url
     * @param $permanently - статус 301 при перенаправлении
     */
    public function redirect($url, $permanently = false)
    {
        if($permanently)
            header('location: ' . URL . $url, true, 301);
        else
            header('location: ' . URL . $url);

        exit();
    }

    /**
     * Call redirect to external url
     *
     * @param $url
     */
    public function redirect_external($url)
    {
        header('location: ' . $url);
        exit();
    }

    /**
     * Call redirect to previous page
     */
    public function redirect_previous()
    {
        if(empty($this->prev_url))
            $this->redirect('/');

        header('location: ' . $this->prev_url);
        exit();
    }

    /**
     * Return Ajax Response
     *
     * @param $status
     * @param $message
     */
    public function message($status, $message)
    {
        exit(json_encode(['status' => $status, 'message' => $message]));
    }

    /**
     * Return Ajax Content
     *
     * @param $content
     */
    public function content($content)
    {
        exit(json_encode(['content' => $content]));
    }

    /**
     * Return Ajax Redirect
     *
     * @param $url
     */
    public function location($url)
    {
        exit(json_encode(['url' => $url]));
    }

}
