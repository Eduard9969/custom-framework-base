<?php

/*
 * Activate debug mode
 */
if (DEBUG_ON)
{

    /*
     * Preventing accidental error output when debug is enabled
     */
    if (DEBUG_MODE == 2)
    {
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
    }

    /**
     * Debugging with stop
     *
     * @param $str
     */
    function dg($str)
    {
        echo "<pre>";
        var_dump($str);
        echo "</pre>";

        exit();
    }

}
