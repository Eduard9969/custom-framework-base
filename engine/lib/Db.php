<?php

namespace engine\lib;

use PDO;
use PDOException;

/**
 * Class Db
 * @package engine\lib
 */
class Db
{

    /**
     * @var PDO
     */
    protected $db;

    /**
     * Db constructor.
     */
    public function __construct()
    {
        /*
         * Include database config
         */
        $config = require 'engine/config/db.php';

        /*
         * PDO connect
         */
        try {
            $dsn = 'mysql:host=' . $config['host'] . ';dbname=' . $config['name'] . ';charset=' . $config['charset'];
            $user = $config['user'];
            $pass = $config['password'];

            $this->db = new PDO($dsn, $user, $pass);
        } catch (PDOException $e) {
            die('Connection failed');
        }
    }

    /**
     * Query execution
     *
     * @param $sql
     * @param array $params
     * @return bool|\PDOStatement
     */
    public function query($sql, $params = [])
    {
        $stmt = $this->db->prepare($sql);

        if(!empty($params))
            foreach ($params as $key => $val)
                $stmt->bindValue(':' . $key, $val);

        return $stmt->execute();
    }

    /**
     * Multiple Query Execution
     *
     * @param $sql
     * @param array $params
     * @return array
     */
    public function row($sql, $params = [])
    {
        $result = $this->query($sql, $params);
        return $result->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Run a query by cell
     *
     * @param $sql
     * @param array $params
     * @return mixed
     */
    public function column($sql, $params = [])
    {
        $result = $this->query($sql, $params);
        return $result->fetchColumn();
    }

    /**
     * Retrieve the last INSERT execution ID
     *
     * @return string
     */
    public function lastInsertId()
    {
        return $this->db->lastInsertId();
    }

}
