<?php

namespace engine\lib;

/**
 * Class TimeOut
 */
class TimeOut
{

    /**
     * @var mixed
     */
    private static $time_start = 0;

    /**
     * TimeOut constructor.
     */
    public function __construct()
    {
        return self::$time_start = microtime(true);
    }

    /**
     * Get Time Out
     *
     * @return float
     */
    public static function getTimeOut()
    {
        $time_now = self::$time_start;
        if(empty($time_now))
            $time_now = microtime(true);

        return round(microtime(true) - $time_now, 5);
    }

}
