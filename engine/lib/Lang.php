<?php


namespace engine\lib;

/**
 * Class Lang
 * @package engine\lib
 */
class Lang
{

    /**
     * @var null Current language
     */
    private static $lang = null;

    /**
     * @var array Array of language abbreviations
     */
    private $lang_shorts = [];

    /**
     * @var array Array of languages
     */
    private $langs = [];

    /**
     * Lang constructor.
     */
    public function __construct() { }

    /**
     * Setting a valid array of language abbreviations
     *
     * @param $array [[alias => title], [alias => title]]
     * @return mixed
     */
    public function setLangShortsTitle($array)
    {
        foreach ($array as $key => $value)
            if(is_string($key)) $this->lang_shorts[] = $key;

        return !empty($this->lang_shorts) ? $this->langs = $array : null;
    }

    /**
     * Setting the current language
     *
     * @param $lang_short
     * @return null
     */
    public function setLang($lang_short)
    {
        return in_array($lang_short, $this->lang_shorts) ? (self::$lang = $lang_short) : $this->setDefaultLang();
    }

    /**
     * Set default language
     * @return mixed|null
     */
    private function setDefaultLang()
    {
        return (isset($this->lang_shorts[0])) ? $this->setLang($this->lang_shorts[0]) : null;
    }

    /**
     * Setting the language from the request array
     *
     * @param $request
     * @return null
     */
    public function setLangFromRequest($request)
    {
        $lang = (isset($request['lang']) || !empty($request['lang'])) ? $request['lang'] : $this->setDefaultLang();

        return $this->setLang($lang);
    }

    /**
     * Returns the current language
     *
     * @return null
     */
    public static function getLang()
    {
        return self::$lang;
    }

}