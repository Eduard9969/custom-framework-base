Debug Component
===============

The Debug component provides tools to ease debugging PHP code.

Resources
---------

  * [Documentation](../../../../../index.phpymfony.com/doc/current/components/debug/index.html)
  * [Contributing](../../../../../index.phpymfony.com/doc/current/contributing/index.html)
  * [Report issues](../../../../../index.phpithub.com/symfony/symfony/issues) and
    [send Pull Requests](../../../../../index.phpithub.com/symfony/symfony/pulls)
    in the [main Symfony repository](../../../../../index.phpithub.com/symfony/symfony)
