<?php

  /*
   *
   * {int:\d+}
   * {string:\w+}
   */

  return [

      /*
       * Main Route
       */
      '' => [
          'controller' => 'main',
          'action'     => 'index',
      ],

      /*
       * Service Route
       */
      'go/{alias:\w+}' => [
          'controller' => 'service',
          'action'     => 'go',
      ],
      'error/{code:\d+}' => [
          'controller' => 'service',
          'action'     => 'error',
      ]

  ];
